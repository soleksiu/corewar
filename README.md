# Corewar.
### Language of development: C.
This is a learning project of Unit Factory coding school, which using
'ecole 42' franchise and same learning programm as the network of innovative
schools across the world in such countries as USA, France, Netherlands.

 The purpose of this project is to create a virtual “arena” in which programs
 will fight against one another (the “Champions”). Also created an assembler
 to compile those Champions as well as a Champion which must be writen in 
 specific assembler language wich created for this project.

 To make life funnier, using the external libraries or frameworks was
forbidden or very limited for student projects. Libft included in repository
was developed by author as another lerning project in school 42. Available
external functions for this project is: open, read, write, close, malloc, free,
perror, strerror and exit. It was allowed to use the ncurses library for
visualisation in this project. All additional stuff needed for project 
realisation student must done by yourself.

NOTE: Project was developed and tested on macOS High Sierra 10.13.3.
Unfortunately I have no ability to test it on other platforms for now.

### My team
This is project requires the team which consist of 4 persons.
Project was developed with cooperation and support of my fellow teammates:
* *Vitalii Sokologorskyi*
* *Denys Petryshyn*
* *Dmytro Tsyvin*

My area of responsibility was visualiser, debuging mode, virtual machine main
cycle,processing of part of the commands, logs, testing and champion`s code.


### Assembler commands
1. **lfork:** means long-fork. 
2. **sti:** Take a registry, and two indexes add the two indexes, and use this result
as an address where the value of the first parameter will be copied.
2. **fork:** Create a new process that will inherit the different states of its
father, except its PC, which will be put at (PC + (1st parameter % IDX_MOD)).
4. **lld:** Means long-load. It the same as ld, but without % IDX_MOD.
5. **ld:** Take an argument and a registry. Load the value of the first argument in
the registry.
6. **add:** Take three registries, add the first two, and place the result in third.
7. **zjmp:** It will take an index and jump to this address if the carry is 1.
8. **sub:** Same as add, but with the opcode est 0b101, and uses a substraction.
9. **ldi:** Uses 2 indexes and 1 registry, adding the first two, treating that like
an address, reading a value of a registry’s size and putting it on the third.
10. **or:** This operation is an bit-to-bit OR, in the same principle of and.
11. **st:** take a registry and a registry or an indirect and store the value of the
registry toward a second argument.
12. **aff:** There is an argument’s coding byte, which is a registry, and its content
is interpreted by the character’s ASCII value to display on the standard output.
13. **live:** The instruction that allows a process to stay alive. It can also record
that the player whose number is the argument is indeed alive. 
14. **xor:** Acts like and with an exclusive OR. 
15. **lldi:** Same as ldi, but does not apply any modulo to the addresses.
16. **and:** Apply an & (bit-to-bit AND) over the first two arguments and store the result
in a registry, which is the third argument. 

## Installation
Use make to compile the project.
```make```
## Usage
Programm 'corewar' is getting the input from file with format '.cor' which can 
be generated from assembler code using asm executable. You can use files from
source/binary/ repository or create your own and compile it using asm.
Create binary code of the champion: 
```./asm champion_name.asm```
Execute with: 
```./corewar champion_name.cor```
## Interface screenshots
### Supported flags
![Screenshot](screenshots/flags.png)
### Visualiser - beginning of the game
![Screenshot](screenshots/begin.png)
### Visualiser - game in progress
![Screenshot](screenshots/in_progress.png)
### Debugging mode
![Screenshot](screenshots/debuging mode.png)
### Dump flag outputs the state of memory at the specific game cycle
![Screenshot](screenshots/dump.png)
### Result of the game
![Screenshot](screenshots/results.png)



